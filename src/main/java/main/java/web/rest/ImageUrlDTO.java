package main.java.web.rest;

public class ImageUrlDTO {

    private int id;
    private String url;

    public ImageUrlDTO(int id, String url) {
        this.id = id;
        this.url = url;
    }

    public ImageUrlDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }




}
