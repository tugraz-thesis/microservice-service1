package main.java.web.rest;

public class AlexaSkillFactDTO {

    @Override
    public String toString() {
        return "AlexaSkillFactDTO{" +
            "id=" + id +
            ", factText='" + factText + '\'' +
            ", id_image_url=" + id_image_url +
            '}';
    }

    private int id;
    private String factText;
    private int id_image_url;


    public AlexaSkillFactDTO() {

    }

    public AlexaSkillFactDTO(int id, String factText, int id_image_url) {
        this.id = id;
        this.factText = factText;
        this.id_image_url = id_image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFactText() {
        return factText;
    }

    public void setFactText(String factText) {
        this.factText = factText;
    }

    public int getId_image_url() {
        return id_image_url;
    }

    public void setId_image_url(int id_image_url) {
        this.id_image_url = id_image_url;
    }


}
