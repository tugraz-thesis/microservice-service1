package main.java.web.rest;

import com.amazon.ask.Skill;
import com.amazon.ask.Skills;
import com.amazon.ask.model.RequestEnvelope;
import com.amazon.ask.model.ResponseEnvelope;
import com.amazon.ask.model.services.Serializer;
import com.amazon.ask.util.JacksonSerializer;
import com.codahale.metrics.annotation.Timed;
import main.java.handler.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;

@RestController
@RequestMapping("/api")
public class AlexaFactService1Resource {

    @PostMapping(path="/get-new-fact", consumes = "application/json", produces = "application/json")
    @Timed
    public ResponseEntity getNewFact(InputStream input) {

        final Serializer serializer = new JacksonSerializer();
        RequestEnvelope requestEnvelope = (RequestEnvelope)serializer.deserialize(input, RequestEnvelope.class);

        Skill mySkill = Skills.standard()
            .addRequestHandlers(
                new CancelandStopIntentHandler(),
                new FactIntentHandler(),
                new HelpIntentHandler(),
                new LaunchRequestHandler(),
                new SessionEndedRequestHandler(),
                new FallBackIntentHandler())
            // Add your skill id below
            .withSkillId("amzn1.ask.skill.4e9e9096-382c-499d-bae7-773054be091a")
            .build();

        ResponseEnvelope response = mySkill.invoke(requestEnvelope);

        String jsonString = serializer.serialize(response);

        return ResponseEntity.ok(jsonString);
    }
}
