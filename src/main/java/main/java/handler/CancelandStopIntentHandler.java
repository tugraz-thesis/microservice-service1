package main.java.handler;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

/*
    Skill Sample Java Fact
    Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 */

public class CancelandStopIntentHandler implements RequestHandler {
    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("AMAZON.StopIntent").or(intentName("AMAZON.CancelIntent")).or(intentName("AMAZON.NoIntent")));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        String speechText = "Goodbye. Come back soon to hear a cryptocurrency fact";
        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("Cryptocurrency Facts", speechText)
                .build();
    }
}
