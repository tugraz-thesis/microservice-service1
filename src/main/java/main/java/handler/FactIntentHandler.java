package main.java.handler;

/*
    Skill Sample Java Fact
    Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 */

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.interfaces.display.*;
import main.java.web.rest.AlexaSkillFactDTO;
import main.java.web.rest.ImageUrlDTO;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.amazon.ask.request.Predicates.intentName;

public class FactIntentHandler implements RequestHandler {

    //private static final Logger log = LoggerFactory.getLogger(FactIntentHandler.class);

    RestTemplate restTemplate = new RestTemplate();

    // via jhipster gateway and public aws dns
    String getCountFactsUrlService2
        = "http://ec2-34-240-250-85.eu-west-1.compute.amazonaws.com:8080/service2/api/alexa-crypto-facts-count";
    String getTextUrlService3
        = "http://ec2-34-240-250-85.eu-west-1.compute.amazonaws.com:8080/service3/api/alexa-crypto-facts";
    String getCountFactsUrlService4
        = "http://ec2-34-240-250-85.eu-west-1.compute.amazonaws.com:8080/service4/api/alexa-fact-image-urls";


    /*
     * via microservice direct (without jhipster gateway)
    String getCountFactsUrlService2
        = "http://localhost:8092/api/alexa-crypto-facts-count";
    String getTextUrlService3
        = "http://localhost:8093/api/alexa-crypto-facts";
    String getCountFactsUrlService4
        = "http://localhost:8094/api/alexa-fact-image-urls";
    */

    public FactIntentHandler() {

    }

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("FactIntent").or(intentName("AMAZON.YesIntent")));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {

        String title = "Cryptocurrency Facts";
        String primaryText = "";
        String imageUrl = "";

        // --------------------------------------------------
        // get random fact index (service 2)

        long size = restTemplate.getForObject(getCountFactsUrlService2, long.class);
        int index = new Random().nextInt(Long.valueOf(size).intValue()) + 1;


        // --------------------------------------------------
        // get fact text (service 3)

        AlexaSkillFactDTO skillFact = restTemplate.getForObject(getTextUrlService3 + "/" + index, AlexaSkillFactDTO.class);

        if (skillFact != null) {
            primaryText = skillFact.getFactText();
        }



        // --------------------------------------------------
        // get image URL (service 4)

        ImageUrlDTO imageDTO = restTemplate.getForObject(getCountFactsUrlService4 + "/" + skillFact.getId_image_url(), ImageUrlDTO.class);


        if (imageDTO != null) {
            imageUrl = imageDTO.getUrl();
        }

        // --------------------------------------------------
        // get image URL (service 5)
        // via S3 bucket link
        // e.g: https://s3-eu-west-1.amazonaws.com/alexaskillimagestorage/images/crypto1.jpg


        // --------------------------------------------------
        // prepare the response JSON for the Alexa device

        String secondaryText = "";
        String speechText = "<speak> " + primaryText + "<break time=\"1s\"/>  Would you like to hear another cryptocurrency fact?" + " </speak>";

        Image image = getImage(imageUrl);

        Template template = getBodyTemplate3(title, primaryText, secondaryText, image);

        // Device supports display interface
        if(null!=input.getRequestEnvelope().getContext().getDisplay()) {
            return input.getResponseBuilder()
                    .withSpeech(speechText)
                    .withSimpleCard(title, primaryText)
                    .addRenderTemplateDirective(template)
                    .withReprompt(speechText)
                    .build();
        } else {
            // Headless device
            return input.getResponseBuilder()
                    .withSpeech(speechText)
                    .withSimpleCard(title, primaryText)
                    .withReprompt(speechText)
                    .build();
        }
    }

    /**
     * Helper method to create a body template 3
     * @param title the title to be displayed on the template
     * @param primaryText the primary text to be displayed on the template
     * @param secondaryText the secondary text to be displayed on the template
     * @param image  the url of the image
     * @return Template
     */
    private Template getBodyTemplate3(String title, String primaryText, String secondaryText, Image image) {
        return BodyTemplate3.builder()
                .withImage(image)
                .withTitle(title)
                .withTextContent(getTextContent(primaryText, secondaryText))
                .build();
    }

    /**
     * Helper method to create the image object for display interfaces
     * @param imageUrl the url of the image
     * @return Image that is used in a body template
     */
    private Image getImage(String imageUrl) {
        List<ImageInstance> instances = getImageInstance(imageUrl);
        return Image.builder()
                .withSources(instances)
                .build();
    }

    /**
     * Helper method to create List of image instances
     * @param imageUrl the url of the image
     * @return instances that is used in the image object
     */
    private List<ImageInstance> getImageInstance(String imageUrl) {
        List<ImageInstance> instances = new ArrayList<>();
        ImageInstance instance = ImageInstance.builder()
                .withUrl(imageUrl)
                .build();
        instances.add(instance);
        return instances;
    }

    /**
     * Helper method that returns text content to be used in the body template.
     * @param primaryText
     * @param secondaryText
     * @return RichText that will be rendered with the body template
     */
    private TextContent getTextContent(String primaryText, String secondaryText) {
        return TextContent.builder()
                .withPrimaryText(makeRichText(primaryText))
                .withSecondaryText(makeRichText(secondaryText))
                .build();
    }

    /**
     * Helper method that returns the rich text that can be set as the text content for a body template.
     * @param text The string that needs to be set as the text content for the body template.
     * @return RichText that will be rendered with the body template
     */
    private RichText makeRichText(String text) {
        return RichText.builder()
                .withText(text)
                .build();
    }

}
